// basic imports
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

//env flags
const NODE_ENV = process.env.NODE_ENV || 'development';
const DEV = (NODE_ENV == 'development');
const PROD = (NODE_ENV == 'production');

const definePluginConfig = {
    "process.env": {
        NODE_ENV: JSON.stringify(NODE_ENV)
    },
    __DEV__: DEV,
    _log: DEV ? function () {
            let _console;
            return (_console = console).log.apply(_console, arguments);
        } : function () {
            return false;
        }
};

console.log('Environment: ' + NODE_ENV);
console.log('Production: ' + PROD);
console.log('Development: ' + DEV);

const plugins = [
    new webpack.DefinePlugin(definePluginConfig),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
];

if (PROD) {
    plugins.push(new BundleAnalyzerPlugin({
        analyzerMode: 'static',
        reportFilename: 'webpack-analyzer-report.html',
        openAnalyzer: true
    }));
}


//main webpack configuration
module.exports = {
    devtool: (DEV) ? 'source-map' : '',

    mode: (DEV) ? 'development' : 'production',

    resolve: {
        extensions: ['.webpack.js', '.web.js', '.ts', '.js', '.min.js', '.jsx']
    },

    entry: {
        // entry points JS files that will be loaded
        view: path.join(__dirname, 'src', 'index.js'),
    },

    output: {
        path: path.join(__dirname, 'public', 'scripts'),
        filename: '[name].js'
    },

    optimization: {
        minimize: true,
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader?cacheDirectory'
            },
            {
                test: /\.(jpg|jpeg|png|gif|svg)$/i,
                loader: 'url-loader?name=./../images/[name].[ext]&limit=' + (32 * 1024) // 32kb - data-url limit for IE
            },
            {
                test: /\.s?css$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                mode: 'global',
                                localIdentName: '[name]-[local]--[hash:base64:5]',
                            }
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true,
                            config: {
                                path: 'postcss.config.js'
                            }
                        }
                    },
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff|ttf|eot|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader?name=fonts/[name].[ext]'
            }
        ]
    },

    plugins: plugins,
    externals: {
        "jive": "jive",
        "jive/gadgets": "gadgets",
        "jive/jquery": "jQuery",
        "jive/osapi": "osapi"
    }
};