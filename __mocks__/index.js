const defaultTrack = {
    "album": {
        "album_type": "ALBUM",
        "artists": [{
            "external_urls": {"spotify": "https://open.spotify.com/artist/776Uo845nYHJpNaStv1Ds4"},
            "href": "https://api.spotify.com/v1/artists/776Uo845nYHJpNaStv1Ds4",
            "id": "776Uo845nYHJpNaStv1Ds4",
            "name": "Jimi Hendrix",
            "type": "artist",
            "uri": "spotify:artist:776Uo845nYHJpNaStv1Ds4"
        }],
        "available_markets": ["AD", "AE", "AR", "AT", "AU", "BE", "BG", "BH", "BO", "BR", "CA", "CH", "CL", "CO", "CR", "CY", "CZ", "DE", "DK", "DO", "DZ", "EC", "EE", "EG", "ES", "FI", "FR", "GB", "GR", "GT", "HK", "HN", "HU", "ID", "IE", "IL", "IN", "IS", "IT", "JO", "JP", "KW", "LB", "LI", "LT", "LU", "LV", "MA", "MC", "MT", "MX", "MY", "NI", "NL", "NO", "NZ", "OM", "PA", "PE", "PH", "PL", "PS", "PT", "PY", "QA", "RO", "SA", "SE", "SG", "SK", "SV", "TH", "TN", "TR", "TW", "US", "UY", "VN", "ZA"],
        "external_urls": {"spotify": "https://open.spotify.com/album/5z090LQztiqh13wYspQvKQ"},
        "href": "https://api.spotify.com/v1/albums/5z090LQztiqh13wYspQvKQ",
        "id": "5z090LQztiqh13wYspQvKQ",
        "images": [{
            "height": 640,
            "url": "https://i.scdn.co/image/bf44daf8c3f35de83e5875bc49791c1347ef36f0",
            "width": 640
        }, {
            "height": 300,
            "url": "https://i.scdn.co/image/b9f988b2164ef879c92175f7414726d77530a3a0",
            "width": 300
        }, {"height": 64, "url": "https://i.scdn.co/image/f4f6903d134ff489abbc7ea5545c6cd3a5cb51df", "width": 64}],
        "name": "Electric Ladyland",
        "release_date": "1968-10-25",
        "release_date_precision": "day",
        "total_tracks": 16,
        "type": "album",
        "uri": "spotify:album:5z090LQztiqh13wYspQvKQ"
    },
    "artists": [{
        "external_urls": {"spotify": "https://open.spotify.com/artist/776Uo845nYHJpNaStv1Ds4"},
        "href": "https://api.spotify.com/v1/artists/776Uo845nYHJpNaStv1Ds4",
        "id": "776Uo845nYHJpNaStv1Ds4",
        "name": "Jimi Hendrix",
        "type": "artist",
        "uri": "spotify:artist:776Uo845nYHJpNaStv1Ds4"
    }],
    "available_markets": ["AD", "AE", "AR", "AT", "AU", "BE", "BG", "BH", "BO", "BR", "CA", "CH", "CL", "CO", "CR", "CY", "CZ", "DE", "DK", "DO", "DZ", "EC", "EE", "EG", "ES", "FI", "FR", "GB", "GR", "GT", "HK", "HN", "HU", "ID", "IE", "IL", "IN", "IS", "IT", "JO", "JP", "KW", "LB", "LI", "LT", "LU", "LV", "MA", "MC", "MT", "MX", "MY", "NI", "NL", "NO", "NZ", "OM", "PA", "PE", "PH", "PL", "PS", "PT", "PY", "QA", "RO", "SA", "SE", "SG", "SK", "SV", "TH", "TN", "TR", "TW", "US", "UY", "VN", "ZA"],
    "disc_number": 1,
    "duration_ms": 240800,
    "explicit": false,
    "external_ids": {"isrc": "USQX90900749"},
    "external_urls": {"spotify": "https://open.spotify.com/track/2aoo2jlRnM3A0NyLQqMN2f"},
    "href": "https://api.spotify.com/v1/tracks/2aoo2jlRnM3A0NyLQqMN2f",
    "id": "2aoo2jlRnM3A0NyLQqMN2f",
    "is_local": false,
    "name": "All Along the Watchtower",
    "popularity": 74,
    "preview_url": "https://p.scdn.co/mp3-preview/5eec2933740ab3984340d5f004813f8275e1bb97?cid=4efa662eae1949d482f61ddfc4f4df18",
    "track_number": 15,
    "type": "track",
    "uri": "spotify:track:2aoo2jlRnM3A0NyLQqMN2f",
    "selected": false
};

export { defaultTrack };
