/**
 * Sets LocalStorage value
 * @param {string}  itemName name
 * @param {any} itemValue value
 * @return {void}
 */
const setLocalStorageParam = (itemName,itemValue) => {
    if(!!window.localStorage){
        localStorage.setItem(itemName, JSON.stringify(itemValue))
    }
}

export default setLocalStorageParam