/**
 * Gets LocalStorage value
 * @param {string}  itemName name
 * @return {any}
 */
const getLocalStorageParam = (itemName) => {
    if(!!window.localStorage){
        const value = localStorage.getItem(itemName)
        return JSON.parse(value)
    }
}

export default getLocalStorageParam