/**
 * @return {object} with current url hash parameters
 */
const getUrlHashParameters = () => {
    return location.hash
        .substring(1)
        .split('&').reduce((result, item) => {
            const parts = item.split('=');
            result[parts[0]] = parts[1];
            return result;
        }, {});
}

export default getUrlHashParameters;