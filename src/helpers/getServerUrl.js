/**
 * Returns server url for current request
 * @param  {object} req current request
 * @return {string} server url
 */
const getServerUrl = (req) => {
    return req.protocol + '://' + req.get('host')
}

module.exports = getServerUrl