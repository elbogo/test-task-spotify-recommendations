import React from 'react';
import styles from './styles.scss';

const Checkbox = ({index,checked, onChange}) => {
    return <div className={styles.Checkbox}>
        <input type="checkbox" id={`btn${index}`} className="btn" checked={!!checked} onChange={onChange}/>
        <label className="check" htmlFor={`btn${index}`}>
            <div className="image"></div>
        </label>
    </div>;
}

export default Checkbox;