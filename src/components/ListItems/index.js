import React from 'react';
import ListItem from '../ListItem';

const ListItems = ({data, toggleTrack, trackType}) => {
    return <div className="list-items">
        {data.length && data.map((track, index) => {
            const {name, artists, album, preview_url, external_urls, selected} = track;
            return <ListItem
                key={`track${index}`}
                index={index}
                {...{name, artists, album, preview_url, external_urls, selected}}
                toggleTrack={() => toggleTrack(index, trackType)}
            />
        })}
    </div>
};

export default ListItems;

