import React from 'react';
import styles from './styles.scss';

const ActionButton = ({text, onClick}) => {
    return <button className={styles.ActionButton} onClick={onClick}>
        {text}
    </button>
}

export default ActionButton;