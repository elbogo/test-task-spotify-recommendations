import React from 'react';
import { shallow } from 'enzyme';
import { findByTestAtrr, checkProps } from './../../../Utils';
import { defaultTrack } from './../../../__mocks__';
import ListItem from './index';

describe('ListItem Component', () => {

    describe('Checking PropTypes', () => {

        it('Should NOT throw a warning', () => {
            const propsError = checkProps(ListItem, defaultTrack);
            expect(propsError).toBeUndefined();
        });

    });

    describe('Component Renders', () => {

        let wrapper;
        beforeEach(() => {
            wrapper = shallow(<ListItem {...defaultTrack} />);
        });

        it('Should renders without error', () => {
            const component = findByTestAtrr(wrapper, 'listItemComponent');
            expect(component.length).toBe(1);
        });


    });


    describe('Should NOT render', () => {

        let wrapper;
        beforeEach(() => {
            const props = {
                test: 'Some text'
            };
            wrapper = shallow(<ListItem {...props} />);
        });

        it('Component is not rendered', () => {
            const component = findByTestAtrr(wrapper, 'listItemComponent');
            expect(component.length).toBe(0);
        });

    });


});

