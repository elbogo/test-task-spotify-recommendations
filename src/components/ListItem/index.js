import React, {Component} from 'react';
import defaultImage from '../../assets/graphics/logo.svg';
import AudioPlayer from 'react-modular-audio-player';
import Checkbox from '../Checkbox';
import style from './styles.scss';

const ListItem = ({index, name, artists, album, preview_url, external_urls, selected = false, toggleTrack}) => {

    const hasArtists = Array.isArray(artists);
    const cover = album && album.images && album.images[0] ? album.images[0].url : defaultImage;
    const hasLinkAndName = !!external_urls && !!name;

    if(!hasLinkAndName) return null;

    return <div className={style.listItem} data-test="listItemComponent">
        <div className="cover" style={{backgroundImage: `url(${cover})`}}></div>
        <div className="info">
            {!!hasLinkAndName && <a className="track-name" href={external_urls.spotify} target="_blank" data-test="trackName">{name}</a>}
            { hasArtists && <ul className="artists">
                {
                    artists.map((artist, index) => <li key={`artist${index}`}>
                        <a href={artist.external_urls.spotify} target="_blank">{artist.name}</a>
                    </li>)
                }
            </ul>}
            {!!preview_url && <AudioPlayer audioFiles={[{src: preview_url}]} hideRewind={true} hideForward={true} hideLoop={true}/>}
        </div>
        <div className="actions">
            <Checkbox index={index} checked={selected} onChange={toggleTrack}/>
        </div>
    </div>;
}

export default ListItem;