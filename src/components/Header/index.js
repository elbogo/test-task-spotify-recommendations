import React from 'react';
import styles from './styles.scss';
import Logo from './../../assets/graphics/logo.svg';

const Header = ({actionButton}) => {
    return <header className={styles.Header} data-test="headerComponent">
        <div className="wrap">
            <div className="logo">
                <img data-test="logoIMG" src={Logo} alt="Logo"/>
            </div>
            <h1>Spotify Recommender</h1>
            {actionButton}
        </div>
    </header>
};

export default Header;