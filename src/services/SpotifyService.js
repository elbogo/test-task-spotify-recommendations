import axios from 'axios';
import getUrlHashParameters from '../helpers/getUrlHashParameters';

class SpotifyService {

    static getTracks(token) {

        const endpointUrl = 'https://api.spotify.com/v1/me/tracks';
        return new Promise(async(resolve) => {

            const tracks = await axios.get(endpointUrl, {headers: {Authorization: `Bearer ${token}`}})
                .then(res => {
                    if(!!res.data && !!res.data.items){
                        resolve(res.data.items);
                    }else{
                        console.error('SpotifyService -> getTracks error: ', err);
                    }
                })
                .catch(err => {
                    console.error('SpotifyService -> getTracks error: ', err);
                })

            resolve(tracks);
        })
    }

    static getRecommendations(token,trackIds) {

        const endpointUrl = 'https://api.spotify.com/v1/recommendations';
        return new Promise(async(resolve) => {

            const tracks = await axios.get(`${endpointUrl}?seed_tracks=${trackIds}`, {headers: {Authorization: `Bearer ${token}`}})
                .then(res => {
                    if(!!res.data && !!res.data.tracks){
                        resolve(res.data.tracks);
                    }else{
                        console.error('SpotifyService -> getRecommendations error: ', err);
                    }
                })
                .catch(err => {
                    console.error('SpotifyService -> getRecommendations error: ', err);
                })

            resolve(tracks);
        })
    }

    static refreshToken(refreshToken) {


        const endpointUrl = '/refresh_token';
        return new Promise(async(resolve) => {

            const result = await axios.get(`${endpointUrl}?refresh_token=${refreshToken}`)
                .then(res => {
                    if(!!res.data && !!res.data.access_token){
                        resolve(res.data.access_token);
                    }else{
                        console.error('SpotifyService -> refreshToken error: ', err);
                    }
                })
                .catch(err => {
                    console.error('SpotifyService -> refreshToken error: ', err);
                })

            resolve(result);
        });
    }

    static getTokens() {
        const {access_token, refresh_token} = getUrlHashParameters()
        return {
            accessToken: access_token,
            refreshToken: refresh_token,
        }
    }
}
;

export default SpotifyService;
