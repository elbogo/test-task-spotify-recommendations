export const types = {
    GET_TRACKS: 'getTracks',
    SET_TRACKS: 'setTracks',
    GET_RECOMMENDATIONS: 'getRecommendations',
    UNSET_RECOMMENDATIONS: 'unsetRecommendations',
    GET_TOKENS: 'getToken',
    GET_ACCESS_TOKEN: 'getAccessToken'
};