import { types } from './types';
import SpotifyService from '../services/SpotifyService';
import getLocalStorageParam from '../helpers/getLocalStorageParam';
import setLocalStorageParam from '../helpers/setLocalStorageParam';

const oneHourMs = 60*60*1000;

export const getTokens = () => async (dispatch) => {

    const isTokenPage = location.href.includes('#access_token');
    let tokens;

    if(isTokenPage) {
        tokens = SpotifyService.getTokens();
        const expiresAt = new Date().getTime() + oneHourMs
        setLocalStorageParam('tokens',{...tokens,expiresAt})
    }else{
        tokens = getLocalStorageParam('tokens');
    }

    const tokensValid = !!tokens && tokens.accessToken && tokens.refreshToken;

    if(tokensValid){
        dispatch({
            type: types.GET_TOKENS,
            payload: tokens
        })
    }

};

export const refreshToken = (refreshToken) => async (dispatch,getState) => {

    const tokens = getState().spotify.tokens;
    const accessToken = await SpotifyService.refreshToken(refreshToken);
    const expiresAt = new Date().getTime() + oneHourMs;

    const newTokens = {
        ...tokens,
        accessToken,
        expiresAt
    }

    setLocalStorageParam('tokens',newTokens);

    return new Promise( resolve => {
        if(!!accessToken){
            dispatch({
                type: types.GET_ACCESS_TOKEN,
                payload: newTokens
            })
        }
        resolve(accessToken)
    })



};

export const getRecommendations = (trackIds) => async (dispatch,getState) => {

    let tokens = getState().spotify.tokens;
    const time = new Date().getTime();

    if (!!tokens && time >= tokens.expiresAt) {
        await refreshToken(tokens.refreshToken)(dispatch,getState);
    } else {
        const tracks = await SpotifyService.getRecommendations(tokens.accessToken,trackIds)

        const tracksValid = Array.isArray(tracks)

        return new Promise( resolve => {

            if (tracksValid) {
                dispatch({
                    type: types.GET_RECOMMENDATIONS,
                    payload: tracks
                })
            }

            resolve(tracks)
        })
    }

};

export const unsetRecommendations = () => async (dispatch) => {

    dispatch({
        type: types.UNSET_RECOMMENDATIONS,
    })

};

export const getTracks = () => async (dispatch,getState) => {

    let tokens = getState().spotify.tokens;
    const time = new Date().getTime();

    if (!!tokens && time >= tokens.expiresAt) {
        await refreshToken(tokens.refreshToken)(dispatch,getState);
    } else {
        const tracks = await SpotifyService.getTracks(tokens.accessToken)

        const tracksValid = Array.isArray(tracks)

        return new Promise( resolve => {

            if (tracksValid) {
                dispatch({
                    type: types.GET_TRACKS,
                    payload: tracks
                })
            }

            resolve(tracks)
        })
    }

};

export const toggleTrack = (index,trackType) => (dispatch,getState) => {
    let tracks = trackType === 'track' ? getState().spotify.tracks : getState().spotify.recommendations

    const newTracks = [...tracks]
    newTracks[index].selected = !newTracks[index].selected

    dispatch({
        type: types.SET_TRACKS,
        payload: {
            newTracks,
            trackType
        }
    })
};