import App from './App';
import { shallow } from 'enzyme';
import { testStore, findByTestAtrr } from './../Utils';
import React from 'react';
import { defaultTrack } from './../__mocks__';

const setUp = (initialState={}) => {
    const store = testStore(initialState);
    const wrapper = shallow(<App store={store} />).childAt(0).dive();
    return wrapper;
};

describe('App Component', () => {

    let wrapper;
    beforeEach(() => {
        const initialState = {
            spotify: {
                recommendations: [],
                tracks: [defaultTrack,defaultTrack,defaultTrack]
            }
        }
        wrapper = setUp(initialState);
    });

    it('App is rendered', () => {
        const component = findByTestAtrr(wrapper, 'App');
        expect(component.length).toBe(1);
    });

});