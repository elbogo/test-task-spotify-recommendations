import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import cn from  'class-names'
import useEffectAsync from './helpers/useEffectAsync';
import Header from './components/Header';
import Headline from './components/Headline';
import ActionButton from './components/ActionButton';
import ListItems from './components/ListItems';

import {getTracks, getRecommendations, unsetRecommendations, getTokens, toggleTrack} from './actions';
import styles from './app.scss';

const App = ({
    getTracks, toggleTrack, tracks,
    getRecommendations, unsetRecommendations, recommendations,
    getTokens, tokens
}) => {

    if (!tokens) {
        getTokens()
    }

    useEffectAsync(async() => {
        if(!!tokens) await getTracks();
    }, [tokens])

    const loggedIn = !!tokens;
    const data = !!recommendations.length ? recommendations : tracks;
    const trackType = !!recommendations.length ? 'recommendation' : 'track'
    const selectedTracks = data.filter(track => track.selected);
    const showRecommededButton = !!selectedTracks.length;
    const showActionsButton = (loggedIn && !!selectedTracks.length) || !!recommendations.length;

    return (
        <div className={styles.App} data-test="App">
            <Header actionButton={showActionsButton && <ActionButton
                onClick={showRecommededButton ? () => getRecommendations(selectedTracks.map(t => t.id)) : unsetRecommendations }
                text={showRecommededButton ? 'Get Recommended' : 'Back'}
            />}/>
            <section className={cn('main',{'tabbed': showActionsButton})}>

                {/* GUEST view */}
                { !loggedIn && <div className="guest">
                    <Headline header="Explore the music!" desc="Please authorize first to start exploring."/>
                    <a href="/login" className="login">Login</a>
                </div>}

                {/* USER view */}
                { loggedIn && <div className="user">
                    <Headline header="Welcome!" desc="Here are your top tracks"/>

                    {!!data.length && <ListItems data={data} toggleTrack={toggleTrack} trackType={trackType}/>}

                </div>}

            </section>
        </div>
    );
}

const mapStateToProps = state => {

    const {spotify} = state;

    return {
        tracks: spotify.tracks,
        recommendations: spotify.recommendations,
        tokens: spotify.tokens
    }
}

export default connect(mapStateToProps, {
    getTracks,
    getRecommendations,
    unsetRecommendations,
    getTokens,
    toggleTrack
})(App);
