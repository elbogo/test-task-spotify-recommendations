import { types } from './../actions/types';

const appDefault = {
    tracks: [],
    recommendations: []
};

export default (state=appDefault, action) => {
    switch(action.type){
        case types.GET_TOKENS:
            return {
                ...state,
                tokens: action.payload
            };
        case types.GET_TRACKS:
            return {
                ...state,
                tracks: action.payload
            };
        case types.SET_TRACKS:
            if(action.payload.trackType === 'recommendation'){
                return {
                    ...state,
                    recommendations: action.payload.newTracks
                };
            }else{
                return {
                    ...state,
                    tracks: action.payload.newTracks
                };
            }
        case types.GET_RECOMMENDATIONS:
            return {
                ...state,
                recommendations: action.payload
            };
        case types.UNSET_RECOMMENDATIONS:
            return {
                ...state,
                tracks: state.tracks.map(track => ({...track,selected: false})),
                recommendations: appDefault.recommendations
            };
        case types.GET_ACCESS_TOKEN:
            return {
                ...state,
                tokens: {
                    ...state.tokens,
                    ...action.payload,
                }
            };
        default:
            return state;
    }
};