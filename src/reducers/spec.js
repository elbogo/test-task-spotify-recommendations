import { types } from './../actions/types';
import spotifyReducer from './../reducers/spotify';
import { defaultTrack } from './../../__mocks__';

describe('Spotify Reducer', () => {

    it('Should return default state', () => {
        const newState = spotifyReducer(undefined, {});
        expect(newState).toEqual({"recommendations": [], "tracks": []});
    });

    it('Should return new state if receiving type', () => {

        const tracks = [defaultTrack,defaultTrack,defaultTrack];
        const newState = spotifyReducer(undefined, {
            type: types.GET_TRACKS,
            payload: tracks
        });
        expect(newState).toEqual({recommendations: [], tracks});

    });

});